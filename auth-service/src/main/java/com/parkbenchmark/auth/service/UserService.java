package com.parkbenchmark.auth.service;

import com.parkbenchmark.auth.domain.User;

public interface UserService {

	void create(User user);

}
