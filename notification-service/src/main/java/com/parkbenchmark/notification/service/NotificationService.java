package com.parkbenchmark.notification.service;

public interface NotificationService {

	void sendBackupNotifications();

	void sendRemindNotifications();
}
