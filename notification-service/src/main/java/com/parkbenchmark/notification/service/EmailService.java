package com.parkbenchmark.notification.service;

import com.parkbenchmark.notification.domain.NotificationType;
import com.parkbenchmark.notification.domain.Recipient;

import javax.mail.MessagingException;
import java.io.IOException;

public interface EmailService {

	void send(NotificationType type, Recipient recipient, String attachment) throws MessagingException, IOException;

}
